/*
 * Copyright 2021 Devin Lin <espidev@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "scclient.h"

User *User::fromJson(const QJsonValue &obj)
{
    User *u = new User();
    u->id = obj["id"].toInt();
    u->permalink = obj["permalink"].toString();
    u->username = obj["username"].toString();
    u->uri = obj["uri"].toString();
    u->permalinkUrl = obj["permalink_url"].toString();
    u->avatarUrl = obj["avatar_url"].toString();
    u->country = obj["country"].toString();
    u->firstName = obj["first_name"].toString(); // likely empty
    u->lastName = obj["last_name"].toString();
    u->fullName = obj["full_name"].toString();
    u->description = obj["description"].toString();
    u->city = obj["city"].toString();
    u->website = obj["website"].toString();
    u->websiteTitle = obj["website_title"].toString();
    u->trackCount = obj["track_count"].toInt();
    u->playlistCount = obj["playlist_count"].toInt();
    u->publicFavoritesCount = obj["public_favorites_count"].toInt();
    u->followersCount = obj["followers_count"].toInt();
    u->followingsCount = obj["followings_count"].toInt();
    u->repostsCount = obj["reposts_count"].toInt();
    return u;
}

Track *Track::fromJson(const QJsonValue &obj)
{
    Track *t = new Track();
    t->id = obj["id"].toInt();
    t->title = obj["title"].toString();
    t->genre = obj["genre"].toString();
    t->description = obj["description"].toString();
    t->artworkUrl = obj["artwork_url"].toString();
    t->tagList = obj["tag_list"].toString();
    t->duration = obj["duration"].toInt();
    t->commentCount = obj["comment_count"].toInt();
    t->repostsCount = obj["reposts_count"].toInt();
    t->favoritingsCount = obj["favoritings_count"].toInt();
    t->playbackCount = obj["playback_count"].toInt();
    t->commentable = obj["commentable"].toBool();
    t->streamable = obj["streamble"].toBool();
    t->createdAt = obj["created_at"].toString();
    t->permalinkUrl = obj["permalink_url"].toString(); // regular url
    t->streamUrl = obj["stream_url"].toString();
    t->waveformUrl = obj["waveform_url"].toString();
    t->userFavourite = obj["user_favourite"].toBool();

    Track::User *user = new Track::User();
    user->id = obj["user"]["id"].toInt();
    user->username = obj["user"]["username"].toString();
    user->uri = obj["user"]["uri"].toString();
    user->permalinkUrl = obj["user"]["permalink_url"].toString();
    user->avatarUrl = obj["user"]["avatar_url"].toString();
    t->user = user;

    return t;
}

SCClient::SCClient(QObject *parent)
{
    m_manager = new QNetworkAccessManager(this);
}

// parse utils
QList<Track *> parseTrackList(QJsonDocument &doc) {
    QList<Track *> list;
    for (const QJsonValue &obj : doc.array()) {
        list.push_back(Track::fromJson(obj));
    }
    return list;
}

QList<User *> parseUserList(QJsonDocument &doc) {
    QList<User *> list;
    for (const QJsonValue &obj : doc.array()) {
        list.push_back(User::fromJson(obj));
    }
    return list;
}

QUrl SCClient::createSimpleQuery(QString url, int limit)
{
    QUrl ret(url);
    QUrlQuery urlQuery;
    urlQuery.addQueryItem(QStringLiteral("client_id"), m_clientId);
    urlQuery.addQueryItem(QStringLiteral("limit"), QString(limit));
    ret.setQuery(urlQuery);
    return ret;
}

// endpoints
SCResponse<QList<Track *>> *SCClient::getSearchTracks(QString &query, int limit)
{
    QUrl url(QStringLiteral("https://api.soundcloud.com/tracks"));
    QUrlQuery urlQuery;
    urlQuery.addQueryItem(QStringLiteral("client_id"), m_clientId);
    urlQuery.addQueryItem(QStringLiteral("q"), query);
    urlQuery.addQueryItem(QStringLiteral("limit"), QString(limit));
    url.setQuery(urlQuery);
    return new SCResponse<QList<Track *>>(this, m_manager->get(QNetworkRequest(url)), parseTrackList);
}
SCResponse<QList<User *>> *SCClient::getSearchUsers(QString &query, int limit)
{
    QUrl url(QStringLiteral("https://api.soundcloud.com/users"));
    QUrlQuery urlQuery;
    urlQuery.addQueryItem(QStringLiteral("client_id"), m_clientId);
    urlQuery.addQueryItem(QStringLiteral("q"), query);
    urlQuery.addQueryItem(QStringLiteral("limit"), QString(limit));
    url.setQuery(urlQuery);
    return new SCResponse<QList<User *>>(this, m_manager->get(QNetworkRequest(url)), parseUserList);
}

SCResponse<Track *> *SCClient::getTrack(QString &trackId)
{
    QUrl url(QStringLiteral("https://api.soundcloud.com/tracks/") + trackId);
    QUrlQuery urlQuery;
    urlQuery.addQueryItem(QStringLiteral("client_id"), m_clientId);
    url.setQuery(urlQuery);
    return new SCResponse<Track *>(this, m_manager->get(QNetworkRequest(url)), [](QJsonDocument &doc) {
        return Track::fromJson(doc.object());
    });
}
SCResponse<QList<User *>> *SCClient::getTrackFavoriters(QString &trackId, int limit)
{
    QUrl url = createSimpleQuery(QStringLiteral("https://api.soundcloud.com/tracks/") + trackId + QStringLiteral("/favoriters"), limit);
    return new SCResponse<QList<User *>>(this, m_manager->get(QNetworkRequest(url)), parseUserList);
}
SCResponse<QList<User *>> *SCClient::getTrackReposters(QString &trackId, int limit)
{
    QUrl url = createSimpleQuery(QStringLiteral("https://api.soundcloud.com/tracks/") + trackId + QStringLiteral("/reposters"), limit);
    return new SCResponse<QList<User *>>(this, m_manager->get(QNetworkRequest(url)), parseUserList);
}
SCResponse<QList<Track *>> *SCClient::getTrackRelated(QString &trackId, int limit)
{
    QUrl url = createSimpleQuery(QStringLiteral("https://api.soundcloud.com/tracks/") + trackId + QStringLiteral("/related"), limit);
    return new SCResponse<QList<Track *>>(this, m_manager->get(QNetworkRequest(url)), parseTrackList);
}

SCResponse<User *> *SCClient::getUser(QString &userId)
{
    QUrl url(QStringLiteral("https://api.soundcloud.com/users/") + userId);
    QUrlQuery urlQuery;
    urlQuery.addQueryItem(QStringLiteral("client_id"), m_clientId);
    url.setQuery(urlQuery);
    return new SCResponse<User *>(this, m_manager->get(QNetworkRequest(url)), [](QJsonDocument &doc) {
        return User::fromJson(doc.object());
    });
}

SCResponse<QList<Track *>> *SCClient::getUserTracks(QString &userId, int limit)
{
    QUrl url = createSimpleQuery(QStringLiteral("https://api.soundcloud.com/users/") + userId + QStringLiteral("/tracks"), limit);
    return new SCResponse<QList<Track *>>(this, m_manager->get(QNetworkRequest(url)), parseTrackList);
}

SCResponse<QList<User *>> *SCClient::getUserFollowers(QString &userId, int limit)
{
    QUrl url = createSimpleQuery(QStringLiteral("https://api.soundcloud.com/users/") + userId + QStringLiteral("/followers"), limit);
    return new SCResponse<QList<User *>>(this, m_manager->get(QNetworkRequest(url)), parseUserList);
}

SCResponse<QList<User *>> *SCClient::getUserFollowings(QString &userId, int limit)
{
    QUrl url = createSimpleQuery(QStringLiteral("https://api.soundcloud.com/users/") + userId + QStringLiteral("/followings"), limit);
    return new SCResponse<QList<User *>>(this, m_manager->get(QNetworkRequest(url)), parseUserList);
}

SCResponse<QList<Track *>> *SCClient::getUserLikes(QString &userId, int limit)
{
    QUrl url = createSimpleQuery(QStringLiteral("https://api.soundcloud.com/users/") + userId + QStringLiteral("/likes/tracks"), limit);
    return new SCResponse<QList<Track *>>(this, m_manager->get(QNetworkRequest(url)), parseTrackList);
}