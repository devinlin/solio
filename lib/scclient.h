/*
 * Copyright 2021 Devin Lin <espidev@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SOLIO_SCCLIENT_H
#define SOLIO_SCCLIENT_H

#include <QString>
#include <QObject>
#include <QNetworkAccessManager>
#include <QUrlQuery>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

#include "scresponse.h"

class User {
public:
    int id;
    QString permalink;
    QString username;
    QString uri;
    QString permalinkUrl;
    QString avatarUrl;
    QString country;
    QString firstName; // likely empty
    QString lastName;
    QString fullName;
    QString description;
    QString city;
    QString website;
    QString websiteTitle;
    int trackCount;
    int playlistCount;
    int publicFavoritesCount;
    int followersCount;
    int followingsCount;
    int repostsCount;

    static User *fromJson(const QJsonValue &obj);
};

class Track {
public:
    struct User {
        int id;
        QString username;
        QString uri;
        QString permalinkUrl;
        QString avatarUrl;
    };

    int id;
    QString title;
    QString genre;
    QString description;
    QString artworkUrl;
    QString tagList;
    int duration;
    int commentCount;
    int repostsCount;
    int favoritingsCount;
    int playbackCount;
    bool commentable;
    bool streamable;
    QString createdAt;
    QString permalinkUrl; // regular url
    QString streamUrl;
    QString waveformUrl;
    User *user = nullptr;
    bool userFavourite;

    ~Track()
    {
        if (user) delete user;
    }
    static Track *fromJson(const QJsonValue &obj);
};

// https://developers.soundcloud.com/docs/api/explorer/open-api#/
class SCClient : QObject {

public:
    SCClient(QObject *parent = nullptr);

    // api.soundcloud.com/tracks
    SCResponse<QList<Track *>> *getSearchTracks(QString &query, int limit = 50);
    // api.soundcloud.com/users
    SCResponse<QList<User *>> *getSearchUsers(QString &query, int limit = 50);

    // api.soundcloud.com/tracks/{track_id}
    SCResponse<Track *> *getTrack(QString &trackId);
    // api.soundcloud.com/tracks/{track_id}/favouriters
    SCResponse<QList<User *>> *getTrackFavoriters(QString &trackId, int limit = 50);
    // api.soundcloud.com/tracks/{track_id}/reposters
    SCResponse<QList<User *>> *getTrackReposters(QString &trackId, int limit = 50);
    // api.soundcloud.com/tracks/{track_id}/related
    SCResponse<QList<Track *>> *getTrackRelated(QString &trackId, int limit = 50);

    // api.soundcloud.com/users/{user_id}
    SCResponse<User *> *getUser(QString &userId);
    // api.soundcloud.com/users/{user_id}/tracks
    SCResponse<QList<Track *>> *getUserTracks(QString &userId, int limit = 50);
    // api.soundcloud.com/users/{user_id}/followers
    SCResponse<QList<User *>> *getUserFollowers(QString &userId, int limit = 50);
    // api.soundcloud.com/users/{user_id}/followings
    SCResponse<QList<User *>> *getUserFollowings(QString &userId, int limit = 50);
    // api.soundcloud.com/users/{user_id}/likes/tracks
    SCResponse<QList<Track *>> *getUserLikes(QString &userId, int limit = 50);

private:
    QUrl createSimpleQuery(QString url, int limit);

    QString m_clientId = "95f22ed54a5c297b1c41f72d713623ef"; // TODO change (from mediaelement)
    QNetworkAccessManager *m_manager;
};


#endif //SOLIO_SCCLIENT_H
