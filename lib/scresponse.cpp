/*
 * Copyright 2021 Devin Lin <espidev@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "scresponse.h"

SCResponse::SCResponse(QObject parent, QNetworkReply *reply, std::function<T()> parser)
{
    m_reply = reply;
    m_parser = parser;
    connect(m_reply, &QNetworkReply::finished, this, &SCResponse::receiveData);
}

void SCResponse::receiveData()
{
    if (reply->error()) {
        qWarning() << "network error when fetching data:" << reply->errorString();
        Q_EMIT(finished(nullptr, reply->error(), reply->errorString()));
    } else {
        QJsonDocument jsonDocument = QJsonDocument::fromJson(reply->readAll());
        if (!jsonDocument.isObject()) {
            qWarning() << "received json is not object!";
        }
        Q_EMIT finished(m_parser(jsonDocument), reply->error(), "");
    }
    m_reply->deleteLater();
}