/*
 * Copyright 2021 Devin Lin <espidev@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SOLIO_SCRESPONSE_H
#define SOLIO_SCRESPONSE_H

#include <QObject>
#include <QNetworkReply>

template <typename T>
class SCResponse : QObject {
public:
    SCResponse(QObject *parent = nullptr) {};
    SCResponse(QObject *parent, QNetworkReply *reply, std::function<T(QJsonDocument&)> parser);

Q_SIGNALS:
    void finished(T *object, QNetworkReply::NetworkError error, QString errorString);

private Q_SLOTS:
    void receiveData();

private:
    std::function<T()> m_parser;
    QNetworkReply *m_reply;
};


#endif //SOLIO_SCRESPONSE_H
