/*
 * Copyright 2021 Devin Lin <espidev@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <KAboutData>
#include <KLocalizedString>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    KLocalizedString::setApplicationDomain("solio");
    KAboutData aboutData("solio",
                         "Solio",
                         QStringLiteral("0.1"),
                         "A native and convergent SoundCloud client.",
                         KAboutLicense::GPL,
                         i18n("© 2021 KDE Community"));
    aboutData.addAuthor(i18n("Devin Lin"), QString(), QStringLiteral("espidev@gmail.com"));
    KAboutData::setApplicationData(aboutData);

    QQmlApplicationEngine *engine = new QQmlApplicationEngine();

    engine->rootContext()->setContextObject(new KLocalizedContext(engine));
    engine->load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}