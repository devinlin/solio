/*
 * Copyright 2021 Devin Lin <espidev@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Controls 2.4
import org.kde.kirigami 2.12 as Kirigami

Kirigami.ApplicationWindow {
    id: appWindow
    minimumWidth: Kirigami.Units.gridUnit * 10
    minimumHeight: Kirigami.Units.gridUnit * 10
    width: Kirigami.Settings.isMobile ? Kirigami.Units.gridUnit * 10 : Kirigami.Units.gridUnit * 20
    height: Kirigami.Settings.isMobile ? Kirigami.Units.gridUnit * 20 : Kirigami.Settings.gridUnit * 10

    title: i18n("Solio")


}